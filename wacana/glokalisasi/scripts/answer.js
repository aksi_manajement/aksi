var tempCorrect = [
    {"number" : 1 , "answer": "Memahami Konsep Glokalisasi"},
    {"number" : 2 , "answer": "Jargon adalah kosakata khusus yang digunakan dalam bidang kehidupan (lingkungan) tertentu"},
    {"number" : 3 , "answer": "Glokalisasi adalah adaptasi produk atau jasa terhadap wilayah atau kebudayaan tempat mereka dijual <br>ATAU<br>Glokalisasi adalah proses-proses global dipengaruhi atau ditumbangkan oleh penerapan, tafsiran, dan adaptasi lokal"},
    {"number" : 4 , "answer": "Berpikir secara mendunia dan mengorbankan kebudayaan lokal"},
    {"number" : 5 , "answer": "Glokalisasi terjadi karena manusia membutuhkan penyesuaian terhadap banyak hal; manusia ingin menjadi bagian dari masyarakat dunia, tetapi tidak ingin kehilangan identitas lokalnya"},
    {"number" : 6 , "answer": "Masyarakat bisa merasakan dampak globalisasi, tetapi masih tetap dekat dengan nuansa lokal"},
    {"number" : 7 , "answer": "Glokalisasi muncul ketika budaya lokal yang dipegang oleh kelompok masyarakat bertemu dengan budaya global yang dibawa oleh sapuan arus globalisasi"},
    {"number" : 8 , "answer": "Makna kalimat tersebut adalah manusia akan susah untuk menerima sesuatu hal yang baru tanpa adanya pengaruh dari kebudayaan yang sudah dipercayainya"},
    {"number" : 9 , "answer": "Glokalisasi pada sushi rendang dilakukan dengan mengganti bagian dalam sushi yang aslinya berupa ikan laut menjadi potongan rendang. Selain itu, lapisan di atas sushi juga diganti dengan potongan daging rendang."},
    {"number" : 10, "answer": "Ya. Karena nasi uduk merupakan makanan khas daerah di Indonesia. Jika dibentuk menjadi sushi, berarti sudah menjadi bagian dari kuliner global."},
    {"number" : 11, "answer": "Chicken Spaghetti Green <br>Spicy Yummy Pasta"},
    {"number" : 12, "answer": "Hal-hal yang terdapat pada music hip hop: DJ (Disk Jokey), MC (Rapp,Rapping), Breakdance (B-boy dan B-girl), dan Graffiti)"},
    {"number" : 13, "answer": "Unsur glokalisasi: lirik berbahasa Jawa, kemeja batik, dan musik tradisional khas Jogja"},
    {"number" : 14, "answer": "Karena musiknya yang cenderung keras dan tegas, serta liriknya yang berisi cacian kepada pemerintah"},
    {"number" : 15, "answer": "Glokalisasi pada Budaya Hip Hop di Jogjakarta"},
    {"number" : 16, "answer": "Karena harus menyesuaikan dengan kebudayaan dan makanan lokal di setiap negara"},
    {"number" : 17, "answer": "Nasi uduk"},
    {"number" : 18, "answer": "Juli"},
    {"number" : 19, "answer": "Perbandingan Penjualan di Restoran Cepat Saji"},
    {"number" : 20, "answer": "Penjualan menu lokal dan menu global memiliki pasarnya masing-masing; ada bulan-bulan yang penjualannya lebih tinggi menu global, dan ada bulan-bulan yang penjualannya lebih tinggi menu lokal."},
];
