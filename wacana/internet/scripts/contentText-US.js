tanslationPages = {
	"screen-2" : {
		"url": "http://www.google.com",
		"tabName": "Google",
		"content" : [
			{"id": "#label_1", "text": "Pencarian Google"},
			{"id": "#label_2", "text": "Google dan logo Google adalah merek dagang terdaftar dari Google Inc., digunakan dengan izin."}
		]
	},
	"screen-3" : {
		"url": "http://www.google.com?q=manfaat+internet",
		"tabName": "Google",
		"content" : [
			{"id": "#search_inpt", "text": "Manfaat internet"},
			{"id": "#label_1", "text": "Memanfaatkan Internet untuk Pengembangan Bisnis| internesia.com"},
			{"id": "#label_2", "text": "https://www.internesia.com &#8250; Internet &#8250; Bisnis"},
			{"id": "#label_3", "text": "22 Des 2017 - Selain mempunyai manfaat untuk menambah wawasan penggunanya, internet juga berguna sebagai sarana pengembangan bisnis ..."},
			{"id": "#label_4", "text": "Sebutkan manfaat jaringan- Brain.co.id"},
			{"id": "#label_5", "text": "https://brain.co.id &#2073; Sekolah Menengah Atas &#8250; Ips"},
			{"id": "#label_6", "text": "Sebutkan manfaat jaringan nirkabel. Sebelum kakak menjawab manfaat jaringan nirkabel, kakak akan menjelaskan pengertian jaringan nirkabel. jaringan nirkabel adalah jaringan ..."},
			{"id": "#label_7", "text": "(PDF) Pemanfaatan Internet Positif dan ..."},
			{"id": "#label_8", "text": "https://www.academi.id/.../Pemanfaatan_Internet_Positif_Dan..."},
			{"id": "#label_9", "text": "Pengertian dan pemanfaatan internet positif lengkap. Seperti yang kita ketahui bahwa internet telah menciptakan revolusi yang sangat tidak terduga dalam dunia ..."},
			{"id": "#label_10", "text": "Tujuan, Manfaat dan Fungsi Internet | FUNGSIKLOPEDIA.COM"},
			{"id": "#label_11", "text": "www.fungsiklopedia.com/fungsi-internet/"},
			{"id": "#label_12", "text": "Fungsi internet secara umum adalah sebagai media informasi, hiburan dan sarana komunikasi. Internet juga memiliki banyak manfaat dalam kehidupan  ..."},
			{"id": "#label_13", "text": "<br><br>Google dan logo Google adalah merek dagang terdaftar dari Google Inc., digunakan dengan izin."}
		]
	},
	"screen-4" : {
		"url": "http://www.google.com?q=manfaat+internet",
		"tabName": "internet dan perkembangan intelegensia anaks",
		"content" : [
			{"id": "#search_inpt", "text": "internet dan perkembangan intelegensia anak"},
			{"id": "#label_1", "text": "Apa Sih Intelegensi Manusia Itu? | Norma.web.id"},
			{"id": "#label_2", "text": "https://norma07dp.wordpress.com/apa-sih-intelegensi-manusia-itu/"},
			{"id": "#label_3", "text": "Cipta rasa dan karsa berkembang berkat kemampuan intelligence manusia. Tetapi, tingkat intelegensi setiap individu sangat berbeda. Dibawah ini akan ..."},
			{"id": "#label_4", "text": "[PDF] Gambaran tingkat inteligensi mahasiswa tahun pertama ... - Neliti"},
			{"id": "#label_5", "text": "https://media.neliti.com/.../63963-ID-gambaran-tingkat-inteligensi-mahasiswa-t.pdf <br> oleh NLHS Montolalu - Dirujuk 1 kali - Artikel terkait"},
			{"id": "#label_6", "text": "Abstrak: Inteligensi adalah salah satu kemampuan mental, pikiran atau intelektual manusia ... Alat tes inteligensi yang digunakan adalah Intelligenz Struktur Test (IST). ..... tentang tes IQ (tes intelegensi). [Internet]. 2014 [Diakses 16 Nov. 2016]."},
			{"id": "#label_7", "text": "Pengertian Kecerdasan Intelektual ~ Fatkhan.web.id"},
			{"id": "#label_8", "text": "fatkhan.web.id/pengertian-kecerdasan-intelektual"},
			{"id": "#label_9", "text": "4 Mei 2018 - Wiliam mengemukakan bahwa inteligensi adalah kemampuan global yang ... Tujuhdimensi menurut Robbins (2001: 58) dalam kecerdasan ..."},
			{"id": "#label_10", "text": "KEMAMPUAN DAN INTELEGENSI | Misbakhudin Munir"},
			{"id": "#label_11", "text": "https://misbakhudinmunir.wordpress.com/2010/07/14/kemampuan-dan-intelegensi/"},
			{"id": "#label_12", "text": "14 Jul 2010 - PENDAHULUAN Intelegensi merupakan kemampuan yang dimiliki oleh setiap insan. Intelegensi ini sangat erat kaitannya dengan kehidupan .."},
			{"id": "#label_13", "text": "Anak dan Internet | Apps4God"},
			{"id": "#label_14", "text": "apps4god.org/artikel/anak-dan-internet"},
			{"id": "#label_15", "text": "20 Okt 2015 - Internet memang merupakan satu bentuk perkembangan teknologi ... diri seorang anak biasanya terdapat tujuh kemampuan (intelegensi)"},
			{"id": "#label_16", "text": "<br><br>Google dan logo Google adalah merek dagang terdaftar dari Google Inc., digunakan dengan izin."}
		]
	},
	"screen-5" : {
		"url": "https://www.manfaat-internet.co.id",
		"tabName": "Langkah-langkah Mengenali Hoaks",
		"content" : [
			{"id": "#label_title", 	"text": "8 Planet"},
			{"id": "#label_home", 	"text": "Laman"},
			{"id": ".label_sun", 	"text": "Matahari"},
			{"id": ".label_mercury", "text": "Merkurius"},
			{"id": ".label_venus", 	"text": "Venus"},
			{"id": ".label_earth", 	"text": "Bumi"},
			{"id": ".label_mars", 	"text": "Mars"},
			{"id": ".label_jupiter", "text": "Jupiter"},
			{"id": ".label_saturn", "text": "Saturnus"},
			{"id": ".label_uranus", "text": "Uranus"},
			{"id": ".label_neptune", "text": "Neptunus"},
			{"id": "#label_1", "text": "Tinjauan tentang"},
			{"id": "#label_2", "text": "Tata Surya"},
			{"id": "#label_3", "text": "Matahari adalah pusat Tata Surya. Semua objek pada tata surya mengitari Matahari."},
			{"id": "#label_4", "text": "Tata Surya memiliki delapan planet. Berdasarkan urutan dari Matahari, planet-planet itu adalah: Merkurius, Venus, Bumi, Mars, Jupiter, Saturnus, Uranus, dan Neptunus.Pada gambar ini, planet-planet itu tampak sangat berdekatan. Padahal sebenarnya planet-planet itu berjarak jutaan mil antara satu sama lain."},
			{"id": "#label_5", "text": "Perkemahan Luar Angkasa!"},
			{"id": "#label_6", "text": "Kemudikan perjalanan menuju bintang-bintang!"},
			{"id": "#label_7", "text": "Meluncur!"}
		]
	},
	"screen-6" : {
		"url": "https://www.internet-dan-perkembangan-intelegensia-anak.co.id",
		"tabName": "internet dan perkembangan intelegensia anak",
		"content" : [
			{"id": "#search_inpt", "text": "internet dan perkembangan intelegensia anak"},
			{"id": "#label_1", "text": "Apa Sih Intelegensi Manusia Itu? | Norma.web.id"},
			{"id": "#label_2", "text": "https://norma07dp.wordpress.com/apa-sih-intelegensi-manusia-itu/"},
			{"id": "#label_3", "text": "Cipta rasa dan karsa berkembang berkat kemampuan intelligence manusia. Tetapi, tingkat intelegensi setiap individu sangat berbeda. Dibawah ini akan ..."},
			{"id": "#label_4", "text": "[PDF] Gambaran tingkat inteligensi mahasiswa tahun pertama ... - Neliti"},
			{"id": "#label_5", "text": "https://media.neliti.com/.../63963-ID-gambaran-tingkat-inteligensi-mahasiswa-t.pdf <br> oleh NLHS Montolalu - ‎Dirujuk 1 kali - ‎Artikel terkait"},
			{"id": "#label_6", "text": "Abstrak: Inteligensi adalah salah satu kemampuan mental, pikiran atau intelektual manusia ... Alat tes inteligensi yang digunakan adalah Intelligenz Struktur Test (IST). ..... tentang tes IQ (tes intelegensi). [Internet]. 2014 [Diakses 16 Nov. 2016]."},
			{"id": "#label_7", "text": "Pengertian Kecerdasan Intelektual ~ Fatkhan.web.id"},
			{"id": "#label_8", "text": "fatkhan.web.id/pengertian-kecerdasan-intelektual"},
			{"id": "#label_9", "text": "4 Mei 2018 - Wiliam mengemukakan bahwa inteligensi adalah kemampuan global yang ... Tujuhdimensi menurut Robbins (2001: 58) dalam kecerdasan ..."},
			{"id": "#label_10", "text": "KEMAMPUAN DAN INTELEGENSI | Misbakhudin Munir"},
			{"id": "#label_11", "text": "https://misbakhudinmunir.wordpress.com/2010/07/14/kemampuan-dan-intelegensi/"},
			{"id": "#label_12", "text": "14 Jul 2010 - PENDAHULUAN Intelegensi merupakan kemampuan yang dimiliki oleh setiap insan. Intelegensi ini sangat erat kaitannya dengan kehidupan .."},
			{"id": "#label_13", "text": "Anak dan Internet | Apps4God"},
			{"id": "#label_14", "text": "apps4god.org/artikel/anak-dan-internet"},
			{"id": "#label_15", "text": "20 Okt 2015 - Internet memang merupakan satu bentuk perkembangan teknologi ... diri seorang anak biasanya terdapat tujuh kemampuan (intelegensi)"},
			{"id": "#label_16", "text": "<br><br>Google dan logo Google adalah merek dagang terdaftar dari Google Inc., digunakan dengan izin."}
		]
	},
	"screen-7" : {
		"url": "https://www.internet.co.id",
		"tabName": "Langkah-langkah Mengenali Hoaks",
		"content" : [
			{"id": "#search_inpt", "text": "Suku Bajo"},
			{"id": "#label_1", "text": "Misteri Suku Bajo Terkuak. Suku yang Disebut Penyelam Terbaik di ..."},
			{"id": "#label_2", "text": "https://www.hipwee.com/.../misteri-suku-bajo-terkuak-suku-yang-d..."},
			{"id": "#label_3", "text": "Bukan kekuatan super dari tokoh komik Marvel atau superhero lain, itu adalah kekuatan misterius yang dimiliki oleh orang-orang <b>Suku Bajo</b> …"},
			{"id": "#label_4", "text": "Melirik Keunikan Suku Bajo Indonesia Yang Hampir Punah "},
			{"id": "#label_5", "text": "www.ark21.com/.../melirik-keunikan-suku-bajo-indonesia-yang-ha..."},
			{"id": "#label_6", "text": "<b>Suku bajo</b> ini merupakan suku yang tidak begitu banyak dikenal, karena keberadaan mereka bisa dibilang cukup langka, dan belum banyak ..."},
			{"id": "#label_7", "text": "Kitabisa! - Renovasi Mesjid terapung Suku Bajo, Wakatobi"},
			{"id": "#label_8", "text": "https://kitabisa.com/mesjidbajowakatobi"},
			{"id": "#label_9", "text": "Mesjid terapung <b>Suku Bajo</b> di Desa Bahari, Wakatobi ini mendesak untuk dibantu demi kenyamanan beribadah masyarakat..."},
			{"id": "#label_10", "text": "Suku Bajo Bisa Menyelam 13 Menit Tanpa Alat, Bagaimana Bisa? "},
			{"id": "#label_11", "text": "https://www.viva.co.id/.../1029439-suku-bajo-bisa-menyelam-13-m..."},
			{"id": "#label_12", "text": "Seperti yang ditunjukkan oleh <b>Suku Bajo</b>, yang memiliki limpa berukuran tidak biasa sebagai akibat dari proses adaptasi genetik selama ..."},
			{"id": "#label_13", "text": "Google dan logo Google adalah merek dagang terdaftar dari Google Inc., digunakan dengan izin."}
		]
	},
	"screen-8" : {
		"url": "https://www.internet.co.id",
		"tabName": "Langkah Penanggulangan Hoaks",
		"content" : [
			{"id": "#label_title", "text": "Program Penjelajahan Mars"},
			{"id": "#label_home", "text": "Laman"},
			{"id": "#label_get", "text": "Menuju Mars"},
			{"id": "#label_mission", "text": "Misi"},
			{"id": "#label_sign", "text": "Mencari Tanda-Tanda Kehidupan"},
			{"id": "#label_rover", "text": "Robot Rover Curiosity"},
			{"id": "#label_1", "text": "Program Penjelajahan Mars"},
			{"id": "#label_2", "text": "Selama berabad-abad, orang-orang berusaha mencari tahu tentang Mars. Berkat kemajuan teknologi, kini kita dapat menjelajahi Mars dari dekat."},
			{"id": "#label_3", "text": "Sejak 1965, para ilmuwan telah meluncurkan lebih dari 40 misi menuju Mars. Meski begitu, perjalanan menuju Planet Merah tetap amat sulit. Lebih banyak kegagalan yang ditemukan daripada kesuksesan."},
			{"id": "#label_4", "text": "Akan tetapi, penjelajahan planet Mars terus berlanjut. Para ilmuwan bertekad untuk mencari tahu jika pernah ada kehidupan di Mars."},
			{"id": "#label_ad1", "text": "KAU BISA MENJADI BINTANG!"},
			{"id": "#label_ad2", "text": "Beri nama sebuah bintang dengan namamu atau temanmu!"},
			{"id": "#label_ad3", "text": "Jadilah Bintang!"}
		]
	},
		"screen-9" : {
			"url": "https://www.internet.co.id",
			"tabName": "Langkah Penanggulangan Hoaks",
			"content" : [
				{"id": "#search_inpt", "text": "Keunikan Suku Bajo"},
				{"id": "#label_1", "text": "Misteri Suku Bajo Terkuak. Suku yang Disebut Penyelam Terbaik di ..."},
				{"id": "#label_2", "text": "https://www.hipwee.com/.../misteri-suku-bajo-terkuak-suku-yang-d..."},
				{"id": "#label_3", "text": "Bukan kekuatan super dari tokoh komik Marvel atau superhero lain, itu adalah kekuatan misterius yang dimiliki oleh orang-orang <b>Suku Bajo</b> …"},
				{"id": "#label_4", "text": "Melirik Keunikan Suku Bajo Indonesia Yang Hampir Punah "},
				{"id": "#label_5", "text": "www.ark21.com/.../melirik-keunikan-suku-bajo-indonesia-yang-ha..."},
				{"id": "#label_6", "text": "<b>Suku bajo</b> ini merupakan suku yang tidak begitu banyak dikenal, karena keberadaan mereka bisa dibilang cukup langka, dan belum banyak ..."},
				{"id": "#label_7", "text": "Kitabisa! - Renovasi Mesjid terapung Suku Bajo, Wakatobi"},
				{"id": "#label_8", "text": "https://kitabisa.com/mesjidbajowakatobi"},
				{"id": "#label_9", "text": "Mesjid terapung <b>Suku Bajo</b> di Desa Bahari, Wakatobi ini mendesak untuk dibantu demi kenyamanan beribadah masyarakat..."},
				{"id": "#label_10", "text": "Suku Bajo Bisa Menyelam 13 Menit Tanpa Alat, Bagaimana Bisa? "},
				{"id": "#label_11", "text": "https://www.viva.co.id/.../1029439-suku-bajo-bisa-menyelam-13-m..."},
				{"id": "#label_12", "text": "Seperti yang ditunjukkan oleh <b>Suku Bajo</b>, yang memiliki limpa berukuran tidak biasa sebagai akibat dari proses adaptasi genetik selama ..."},
				{"id": "#label_13", "text": "Google dan logo Google adalah merek dagang terdaftar dari Google Inc., digunakan dengan izin."}
			]
	},
		"screen-10" : {
		"url": "https://www.internet.co.id",
		"tabName": "Langkah Penanggulangan Hoaks",
		"content" : [
			{"id": "#label_title", "text": "Program Penjelajahan Mars"},
			{"id": "#label_home", "text": "Laman"},
			{"id": "#label_get", "text": "Menuju Mars"},
			{"id": "#label_mission", "text": "Misi"},
			{"id": "#label_sign", "text": "Mencari Tanda-Tanda Kehidupan"},
			{"id": "#label_rover", "text": "Robot Rover Curiosity"},
			{"id": "#label_1", "text": "Misi"},
			{"id": "#label_2", "text": "Selama bertahun-tahun, para ilmuwan telah meluncurkan tiga jenis misi ke Mars."},
			{"id": "#label_3", "text": "Flyby (lintas terbang)"},
			{"id": "#label_4", "text": "Misi-misi awal hanya melintasi Mars. Mereka menjepret sebanyak mungkin foto selagi lewat."},
			{"id": "#label_5", "text": "Orbiter (pesawat luar angkasa)"},
			{"id": "#label_6", "text": "Pada tahun 2000, negara-negara telah bisa menaruh pesawat antariksa ke dalam orbit Mars dan mengitarinya. Sejumlah penelitian jangka-panjang kini memungkinkan dilaksanakan. Hari ini, sejumlah pesawat antariksa masih mengelilingi Mars."},
			{"id": "#label_7", "text": "Rover"},
			{"id": "#label_8", "text": "Baru-baru ini, para ilmuwan memikirkan berbagai cara untuk menaruh robot-robot rover ke Mars. Rover adalah sebuah kendaraan beroda-enam  dengan kendali jarak-jauh. Ukurannya seperti sebuah mobil kecil. Ia dapat berjalan berkeliling dan menjelajahi permukaan Mars."},
			{"id": "#label_ad1", "text": "Berjalanlah"},
			{"id": "#label_ad2", "text": "Dan Lihatlah Dunia"},
			{"id": "#label_ad3", "text": "Kehidupan di Stasiun Luar Angkasa"}
		]
	},
		"screen-11" : {
		"url": "https://www.internet.co.id",
		"tabName": "Mengenal Sejarah Pasar Tradisional",
		"content" : [
			{"id": "#label_title", "text": "Program Penjelajahan Mars"},
			{"id": "#label_home", "text": "Laman"},
			{"id": "#label_get", "text": "Menuju Mars"},
			{"id": "#label_mission", "text": "Misi"},
			{"id": "#label_sign", "text": "Mencari Tanda-Tanda Kehidupan"},
			{"id": "#label_rover", "text": "Robot Rover Curiosity"},
			{"id": "#label_1", "text": "Mencari Tanda-Tanda Kehidupan"},
			{"id": "#label_2", "text": "Sebagian besar misi ke Mars mempunyai tujuan yang sama: mencari tanda-tanda kehidupan."},
			{"id": "#label_3", "text": "Salah satu tanda kehidupan adalah air. Karena semua makhluk hidup memerlukan air, para ilmuwan mengirimkan rover pertama kali untuk mencari keberadaan air. Tahun 2012, rover bernama Curiosity mendarat di Mars."},
			{"id": "#label_4", "text": "Foto Curiosity di Mars."},
			{"id": "#label_ad1", "text": "Berjalanlah"},
			{"id": "#label_ad2", "text": "Dan Lihatlah Dunia"},
			{"id": "#label_ad3", "text": "Kehidupan di Stasiun Luar Angkasa"}
		]
	},
		"screen-12" : {
		"url": "https://www.internet.co.id",
		"tabName": "Mengenal Sejarah Pasar Tradisional",
		"content" : [
			{"id": "#label_title", "text": "Program Penjelajahan Mars"},
			{"id": "#label_home", "text": "Laman"},
			{"id": "#label_get", "text": "Menuju Mars"},
			{"id": "#label_mission", "text": "Misi"},
			{"id": "#label_sign", "text": "Mencari Tanda-Tanda Kehidupan"},
			{"id": "#label_rover", "text": "Robot Rover Curiosity"},
			{"id": "#label_1", "text": "Robot Rover Curiosity:<div data-group=\"Page Content\" id=\"label_2\" style=\"display: inline;color: black;font-size: 14px;\"> Seperti manusia, Curiosity memiliki berbagai anggota badan. Hal ini membantu Rover menjelajahi permukaan Mars hampir menyerupai seorang manusia.</div>"},
			{"id": "#label_2", "text": "Seperti manusia, Curiosity memiliki berbagai anggota badan. Hal ini membantu Rover menjelajahi permukaan Mars hampir menyerupai seorang manusia."},
			{"id": "#label_3", "text": "Lengan dan Tangan"},
			{"id": "#label_4", "text": "Tubuh"},
			{"id": "#label_5", "text": "Mata"},
			{"id": "#label_6", "text": "Roda dan Kaki"},
			{"id": "#label_7", "text": "Curiosity memiliki lengan dan tangan robot. Ia memegang dan menggunakan alat agar dapat mengumpulkan sampel-sampel batu dan debu."},
			{"id": "#label_8", "text": "Tubuh Curiosity memiliki banyak instrumen sains. Ia bahkan dapat �melahap� batu-batuan. Alat di tangan rover akan �mengunyah� sampel batu-batuan hingga menjadi bubuk. Selanjutnya, ia akan menuangkan bubuk itu ke dalam tubuhnya. Kemudian, instrumen-instrumen rover akan mengidentifikasi kandungan batu-batu itu."},
			{"id": "#label_9", "text": "Curiosity memiliki 17 kamera. Dua belas kamera membantunya �melihat� ke mana ia pergi.  Ia menggunakan lima kamera lain guna mengambil foto-foto untuk kepentingan investigasi sains."},
			{"id": "#label_10", "text": "Rover mempunyai enam roda dan kaki. Hal ini mampu menjaga keseimbangannya saat Curiosity melintasi bebatuan dan mencegah rover jatuh terbalik."},
			{"id": "#label_ad1", "text": "Berjalanlah"},
			{"id": "#label_ad2", "text": "Dan Lihatlah Dunia"},
			{"id": "#label_ad3", "text": "Kehidupan di Stasiun Luar Angkasa"}
		]
	},
		"screen-13" : {
		"url": "https://www.internet.co.id",
		"tabName": "Mengenal Sejarah Pasar Tradisional",
		"content" : [
			{"id": "#label_title", "text": "Times-Journal"},
			{"id": "#label_1", "text": "Hadiah Curiosity  (Keingintahuan)"},
			{"id": "#label_2", "text": "Oleh Maria Green"},
			{"id": "#label_3", "text": "Selama puluhan tahun, para ilmuwan bertanya-tanya jika permukaan Mars pernah memiliki air. Kini, kamera-kamera Curiosity menunjukkan tanda-tanda bahwa Mars pernah memiliki sungai-sungai. Terdapat foto-foto yang tampak seperti sebuah palung kering. Foto-foto lain menampilkan ngarai-ngarai raksasa  dan lembah-lembah yang mungkin tercipta dari adanya sungai-sungai."},
			{"id": "#label_4", "text": "Ilmuwan kini �meyakini Mars pernah memiliki laut miliaran tahun silam,� ujar Charles Elachi, yang memimpin misi Curiosity."},
			{"id": "#label_5", "text": "Namun jika Mars pernah memiliki laut purba, pertanyaan lain mengemuka, ujar Elachi: \�Apakah kehidupan telah berevolusi di Mars?\� Dan jika itu terjadi: \�Apakah makhluk hidup juga berevolusi? Dan di manakah kehidupan itu sekarang?\�"},
			{"id": "#label_6", "text": "Batu-batu Mars dari sebuah palung kering."},
			{"id": "#label_7", "text": "Batu-batu Bumi dari sebuah palung kering."},
			{"id": "#label_ad1", "text": "Tenaga Solar"},
			{"id": "#label_ad2", "text": "Meng-<br/>hangatkan dan Menerangi Rumah Kita dengan Matahari!"},
			{"id": "#label_ad3", "text": "Kekuatan Matahari!"}
		]
	},
	"screen-14" : {
		"url": "https://www.internet.co.id",
		"tabName": "Mengenal Sejarah Pasar Tradisional",
		"content" : [
			{"id": "#label_title", "text": "Times-Journal"},
			{"id": "#label_1", "text": "Hadiah Curiosity  (Keingintahuan)"},
			{"id": "#label_2", "text": "Oleh Maria Green"},
			{"id": "#label_3", "text": "Selama puluhan tahun, para ilmuwan bertanya-tanya jika permukaan Mars pernah memiliki air. Kini, kamera-kamera Curiosity menunjukkan tanda-tanda bahwa Mars pernah memiliki sungai-sungai. Terdapat foto-foto yang tampak seperti sebuah palung kering. Foto-foto lain menampilkan ngarai-ngarai raksasa  dan lembah-lembah yang mungkin tercipta dari adanya sungai-sungai."},
			{"id": "#label_4", "text": "Ilmuwan kini �meyakini Mars pernah memiliki laut miliaran tahun silam,� ujar Charles Elachi, yang memimpin misi Curiosity."},
			{"id": "#label_5", "text": "Namun jika Mars pernah memiliki laut purba, pertanyaan lain mengemuka, ujar Elachi: \�Apakah kehidupan telah berevolusi di Mars?\� Dan jika itu terjadi: \�Apakah makhluk hidup juga berevolusi? Dan di manakah kehidupan itu sekarang?\�"},
			{"id": "#label_6", "text": "Batu-batu Mars dari sebuah palung kering."},
			{"id": "#label_7", "text": "Batu-batu Bumi dari sebuah palung kering."},
			{"id": "#label_ad1", "text": "Tenaga Solar"},
			{"id": "#label_ad2", "text": "Meng-<br/>hangatkan dan Menerangi Rumah Kita dengan Matahari!"},
			{"id": "#label_ad3", "text": "Kekuatan Matahari!"}
		]
	},
	"screen-15" : {
		"url": "https://www.internet.co.id",
		"tabName": "Mengenal Sejarah Pasar Tradisional",
		"content" : [
			{"id": "#label_title", "text": "Times-Journal"},
			{"id": "#label_1", "text": "Hadiah Curiosity  (Keingintahuan)"},
			{"id": "#label_2", "text": "Oleh Maria Green"},
			{"id": "#label_3", "text": "Selama puluhan tahun, para ilmuwan bertanya-tanya jika permukaan Mars pernah memiliki air. Kini, kamera-kamera Curiosity menunjukkan tanda-tanda bahwa Mars pernah memiliki sungai-sungai. Terdapat foto-foto yang tampak seperti sebuah palung kering. Foto-foto lain menampilkan ngarai-ngarai raksasa  dan lembah-lembah yang mungkin tercipta dari adanya sungai-sungai."},
			{"id": "#label_4", "text": "Ilmuwan kini �meyakini Mars pernah memiliki laut miliaran tahun silam,� ujar Charles Elachi, yang memimpin misi Curiosity."},
			{"id": "#label_5", "text": "Namun jika Mars pernah memiliki laut purba, pertanyaan lain mengemuka, ujar Elachi: \�Apakah kehidupan telah berevolusi di Mars?\� Dan jika itu terjadi: \�Apakah makhluk hidup juga berevolusi? Dan di manakah kehidupan itu sekarang?\�"},
			{"id": "#label_6", "text": "Batu-batu Mars dari sebuah palung kering."},
			{"id": "#label_7", "text": "Batu-batu Bumi dari sebuah palung kering."},
			{"id": "#label_ad1", "text": "Tenaga Solar"},
			{"id": "#label_ad2", "text": "Meng-<br/>hangatkan dan Menerangi Rumah Kita dengan Matahari!"},
			{"id": "#label_ad3", "text": "Kekuatan Matahari!"}
		]
	},
	"screen-16" : {
		"url": "https://www.internet.co.id",
		"tabName": "Mengenal Sejarah Pasar Tradisional",
		"content" : [
			{"id": "#label_title", "text": "Times-Journal"},
			{"id": "#label_1", "text": "Hadiah Curiosity  (Keingintahuan)"},
			{"id": "#label_2", "text": "Oleh Maria Green"},
			{"id": "#label_3", "text": "Selama puluhan tahun, para ilmuwan bertanya-tanya jika permukaan Mars pernah memiliki air. Kini, kamera-kamera Curiosity menunjukkan tanda-tanda bahwa Mars pernah memiliki sungai-sungai. Terdapat foto-foto yang tampak seperti sebuah palung kering. Foto-foto lain menampilkan ngarai-ngarai raksasa  dan lembah-lembah yang mungkin tercipta dari adanya sungai-sungai."},
			{"id": "#label_4", "text": "Ilmuwan kini �meyakini Mars pernah memiliki laut miliaran tahun silam,� ujar Charles Elachi, yang memimpin misi Curiosity."},
			{"id": "#label_5", "text": "Namun jika Mars pernah memiliki laut purba, pertanyaan lain mengemuka, ujar Elachi: \�Apakah kehidupan telah berevolusi di Mars?\� Dan jika itu terjadi: \�Apakah makhluk hidup juga berevolusi? Dan di manakah kehidupan itu sekarang?\�"},
			{"id": "#label_6", "text": "Batu-batu Mars dari sebuah palung kering."},
			{"id": "#label_7", "text": "Batu-batu Bumi dari sebuah palung kering."},
			{"id": "#label_ad1", "text": "Tenaga Solar"},
			{"id": "#label_ad2", "text": "Meng-<br/>hangatkan dan Menerangi Rumah Kita dengan Matahari!"},
			{"id": "#label_ad3", "text": "Kekuatan Matahari!"}
		]
	},
	"screen-17" : {
		"url": "https://www.internet.co.id",
		"tabName": "Mengenal Sejarah Pasar Tradisional",
		"content" : [
			{"id": "#label_title", "text": "Times-Journal"},
			{"id": "#label_1", "text": "Hadiah Curiosity  (Keingintahuan)"},
			{"id": "#label_2", "text": "Oleh Maria Green"},
			{"id": "#label_3", "text": "Selama puluhan tahun, para ilmuwan bertanya-tanya jika permukaan Mars pernah memiliki air. Kini, kamera-kamera Curiosity menunjukkan tanda-tanda bahwa Mars pernah memiliki sungai-sungai. Terdapat foto-foto yang tampak seperti sebuah palung kering. Foto-foto lain menampilkan ngarai-ngarai raksasa  dan lembah-lembah yang mungkin tercipta dari adanya sungai-sungai."},
			{"id": "#label_4", "text": "Ilmuwan kini �meyakini Mars pernah memiliki laut miliaran tahun silam,� ujar Charles Elachi, yang memimpin misi Curiosity."},
			{"id": "#label_5", "text": "Namun jika Mars pernah memiliki laut purba, pertanyaan lain mengemuka, ujar Elachi: \�Apakah kehidupan telah berevolusi di Mars?\� Dan jika itu terjadi: \�Apakah makhluk hidup juga berevolusi? Dan di manakah kehidupan itu sekarang?\�"},
			{"id": "#label_6", "text": "Batu-batu Mars dari sebuah palung kering."},
			{"id": "#label_7", "text": "Batu-batu Bumi dari sebuah palung kering."},
			{"id": "#label_ad1", "text": "Tenaga Solar"},
			{"id": "#label_ad2", "text": "Meng-<br/>hangatkan dan Menerangi Rumah Kita dengan Matahari!"},
			{"id": "#label_ad3", "text": "Kekuatan Matahari!"}
		]
	},
	"screen-18" : {
		"url": "https://www.internet.co.id",
		"tabName": "Mengenal Sejarah Pasar Tradisional",
		"content" : [
			{"id": "#label_title", "text": "Times-Journal"},
			{"id": "#label_1", "text": "Hadiah Curiosity  (Keingintahuan)"},
			{"id": "#label_2", "text": "Oleh Maria Green"},
			{"id": "#label_3", "text": "Selama puluhan tahun, para ilmuwan bertanya-tanya jika permukaan Mars pernah memiliki air. Kini, kamera-kamera Curiosity menunjukkan tanda-tanda bahwa Mars pernah memiliki sungai-sungai. Terdapat foto-foto yang tampak seperti sebuah palung kering. Foto-foto lain menampilkan ngarai-ngarai raksasa  dan lembah-lembah yang mungkin tercipta dari adanya sungai-sungai."},
			{"id": "#label_4", "text": "Ilmuwan kini �meyakini Mars pernah memiliki laut miliaran tahun silam,� ujar Charles Elachi, yang memimpin misi Curiosity."},
			{"id": "#label_5", "text": "Namun jika Mars pernah memiliki laut purba, pertanyaan lain mengemuka, ujar Elachi: \�Apakah kehidupan telah berevolusi di Mars?\� Dan jika itu terjadi: \�Apakah makhluk hidup juga berevolusi? Dan di manakah kehidupan itu sekarang?\�"},
			{"id": "#label_6", "text": "Batu-batu Mars dari sebuah palung kering."},
			{"id": "#label_7", "text": "Batu-batu Bumi dari sebuah palung kering."},
			{"id": "#label_ad1", "text": "Tenaga Solar"},
			{"id": "#label_ad2", "text": "Meng-<br/>hangatkan dan Menerangi Rumah Kita dengan Matahari!"},
			{"id": "#label_ad3", "text": "Kekuatan Matahari!"}
		]
	}
};

tanslationInfo = {
	"incompleteMSG" :{
		"text" : "Jawaban belum dipilih. Apakah kamu yakin mau melanjutkan?"
	},
	"incompleteMSG2" :{
		"text" : "Kolom isian belum diisi. Apakah kamu yakin mau melanjutkan?"
	},
	"incompleteMSG3" :{
		"text" : "Terdapat beberapa kolom isian yang belum diisi. Apakah kamu yakin mau melanjutkan?"
	},
	"wrongWeb" :{
		"text" : "Halaman situs yang benar akan dimuat untukmu."
	},
	"moreTime" :{
		"text" : "Apakah kamu perlu tambahan waktu untuk pertanyaan ini?"
	},
	"moveOn" :{
		"text" : "Kita lanjutkan sekarang!"
	},
	"fiveMin" :{
		"text" : "Waktumu 5 menit lagi"
	},
	"oneMin" :{
		"text" : "Waktumu 1 menit lagi"
	},
	"timesUp" :{
		"text" : "Terima kasih! Waktu habis!"
	}
};

tanslationQuestions = {};
